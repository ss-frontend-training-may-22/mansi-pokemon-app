import { get } from "../Util/ApiManager";
import { PokemomInfoEndPoint } from "../Util/Endpoint";

const getPokemonInfo = (id: number) => {
  return get(PokemomInfoEndPoint(id + 1));
};

export { getPokemonInfo };
