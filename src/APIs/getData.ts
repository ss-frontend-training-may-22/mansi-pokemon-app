import { get } from "../Util/ApiManager";

const getData = (url: string) => {
  return get(url);
};

export { getData };
