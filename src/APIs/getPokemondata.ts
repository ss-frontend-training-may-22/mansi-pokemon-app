import { get } from "../Util/ApiManager";
import { BASE_URL } from "../Util/constants";

const getPokemonData = () => {
  return get(BASE_URL);
};

export { getPokemonData };
