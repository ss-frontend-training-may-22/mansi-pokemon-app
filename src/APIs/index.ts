import { getPokemonData } from "./getPokemondata";
import { getPokemonInfo } from "./getPokemonInfo";
import { getData } from "./getData";

export { getPokemonData, getPokemonInfo, getData };
