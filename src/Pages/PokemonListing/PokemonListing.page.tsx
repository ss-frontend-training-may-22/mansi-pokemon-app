import React from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { getPokemonData } from "../../APIs";
import { PokemonPropsType } from "../../Models";
import {
  CardComponent,
  DropdownComponent,
  InputComponent,
} from "../../stories";
import StylePokemonListPage from "/home/mansi/pokemon-app/src/Pages/PokemonListing/PokemonListing.module.scss";
const PokemonListPage = () => {
  const [pokemonList, setPokemonList] = useState<PokemonPropsType[]>([]);
  const navigate = useNavigate();

  const [count, setCount] = React.useState(10);

  function handleChange(event: any) {
    setCount(event.target.value);
    console.log(count);
  }

  useEffect(() => {
    getPokemonData().then((Response) => {
      setPokemonList(Response.data["results"]);
    });
  }, []);

  return (
    <div className={StylePokemonListPage.pokemonListPage}>
      <div>
        <div className={StylePokemonListPage.pokemonListPage__header}>
          <h1>Welcome to Pokemon Application</h1>
        </div>
        <div className={StylePokemonListPage.pokemonListPage__filtter}>
          <InputComponent placeholder="Search..." onBlur={() => {}} />
          <DropdownComponent onChange={handleChange} count={count} />
        </div>
      </div>
      <div className={StylePokemonListPage.pokemonListPage__gridLayout}>
        {pokemonList
          .slice(0, count)
          .map((pokemon_info: { name: string; url: string; id: number }) => (
            <CardComponent
              name={pokemon_info.name}
              url={pokemon_info.url}
              onClick={() => {
                navigate("/PokemonDetails", {
                  state: { name: pokemon_info.name, url: pokemon_info.url },
                });
              }}
            />
          ))}
      </div>
    </div>
  );
};

export default PokemonListPage;
