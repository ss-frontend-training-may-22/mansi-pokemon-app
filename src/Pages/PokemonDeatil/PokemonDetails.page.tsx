import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { getData } from "../../APIs";
import { PokemonInfoPropsType } from "../../Models";
import { ButtonComponent } from "../../stories";
import { POKEMON_LIST_PAGE_ROUTE } from "../../Util/routes";
import StyledPokemonDetailPage from "/home/mansi/pokemon-app/src/Pages/PokemonDeatil/PokemonDetail.module.scss";

const PokemonDetails = () => {
  const [pokemon, setPokemon] = useState<PokemonInfoPropsType>();

  const { state } = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    getData(state.url).then((res) => setPokemon({ ...pokemon, ...res.data }));
  }, []);

  return (
    <div className={StyledPokemonDetailPage.pokemonDetail}>
      <div>
        <div className={StyledPokemonDetailPage.pokemonDetail__header}>
          <h1>Welcome to Pokemon Application</h1>
        </div>
        <div>
          <h2>Pokemon Details</h2>
        </div>
        <ButtonComponent
          label="Go Back"
          onClick={() => navigate(POKEMON_LIST_PAGE_ROUTE)}
        />
        <div>
          <img src={pokemon?.sprites.other.dream_world.front_default} alt="" />
        </div>
        <div className={StyledPokemonDetailPage.pokemonDetail__information}>
          <div className={StyledPokemonDetailPage.pokemonDetail__point}>
            <label>Name:</label>
            <span>{state.name}</span>
          </div>
          <div className={StyledPokemonDetailPage.pokemonDetail__point}>
            <label>Height:</label>
            <span>{pokemon?.height}</span>
          </div>
          <div className={StyledPokemonDetailPage.pokemonDetail__point}>
            <label>Weight:</label>
            <span>{pokemon?.weight}</span>
          </div>
          <div className={StyledPokemonDetailPage.pokemonDetail__point}>
            <label>Base Experience:</label>
            <span>{pokemon?.base_experience}</span>
          </div>
          <div className={StyledPokemonDetailPage.pokemonDetail__point}>
            <label>Order:</label>
            <span>{pokemon?.order}</span>
          </div>
          <div className={StyledPokemonDetailPage.pokemonDetail__point}>
            <label>List of abilites:</label>
          </div>

          <div className={StyledPokemonDetailPage.pokemonDetail__point}>
            <ol type="1">
              {pokemon?.abilities.map((item) => (
                <li>{item.ability.name}</li>
              ))}
            </ol>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PokemonDetails;
