import { Form, Field } from "react-final-form";
import { Link, useNavigate } from "react-router-dom";
import { FormType, UserDataType } from "../../Models";
import { ButtonComponent, InputComponent } from "../../stories";
import { EMAIL_MESSAGE, PASSWORD_MESSAGE } from "../../Util/constants";
import {
  POKEMON_LIST_PAGE_ROUTE,
  REGISTER_PAGE_ROUTE,
} from "../../Util/routes";
import StyleLoginPage from "./Loginpage.module.scss";

export type LoginType = {
  loginState: boolean;
  setLoginState: (
    loginState: boolean | ((prevVar: boolean) => boolean)
  ) => void;
};

function Loginpage({ loginState, setLoginState }: LoginType) {
  const navigate = useNavigate();

  const onSubmit = (e: FormType) => {
    const allUsers = localStorage.getItem("users");

    if (allUsers) {
      const convertedUser = JSON.parse(allUsers);
      convertedUser.map((user: UserDataType) => {
        if (user.email === e.useremail && user.password === e.password) {
          localStorage.setItem("login", (!loginState).toString());
          setLoginState(!loginState);
          navigate(POKEMON_LIST_PAGE_ROUTE);
        }
      });
    }
  };

  const validate = (e?: FormType) => {
    const errors: FormType = {};

    if (!e?.useremail) {
      errors.useremail = `${EMAIL_MESSAGE}`;
    }

    if (!e?.password) {
      errors.password = `${PASSWORD_MESSAGE}`;
    }

    return errors;
  };

  return (
    <div className={StyleLoginPage.login}>
      <div className={StyleLoginPage.login__form}>
        <h2>Login</h2>
        <Form
          onSubmit={onSubmit}
          validate={validate}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <div className={StyleLoginPage.login__formcontrol}>
                <Field name="useremail">
                  {({ input, meta }) => (
                    <div>
                      <label>Email</label>
                      <InputComponent id="Email" type="email" {...input} />
                      {meta.error && meta.touched && (
                        <span className={StyleLoginPage.login__error}>
                          {meta.error}
                        </span>
                      )}
                    </div>
                  )}
                </Field>
              </div>

              <div className={StyleLoginPage.login__formcontrol}>
                <Field name="password">
                  {({ input, meta }) => (
                    <div>
                      <label>Password</label>
                      <InputComponent
                        id="password"
                        type="password"
                        {...input}
                      />
                      {meta.error && meta.touched && (
                        <span className={StyleLoginPage.login__error}>
                          {meta.error}
                        </span>
                      )}
                    </div>
                  )}
                </Field>
              </div>

              <div className={StyleLoginPage.login__formcontrol}>
                <ButtonComponent
                  label={"Login"}
                  onClick={() => {
                    navigate(POKEMON_LIST_PAGE_ROUTE);
                  }}
                />
              </div>
            </form>
          )}
        />

        <Link to={REGISTER_PAGE_ROUTE} className={StyleLoginPage.login__link}>
          New user? Register
        </Link>
      </div>
    </div>
  );
}

export default Loginpage;
