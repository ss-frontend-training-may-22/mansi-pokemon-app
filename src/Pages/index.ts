import Loginpage from "./Loginpage";
import Registerpage from "./Registerpage";
import PokemonListing from "./PokemonListing";
import { PokemonDetails } from "./PokemonDeatil";

export { Loginpage, Registerpage, PokemonListing, PokemonDetails };
