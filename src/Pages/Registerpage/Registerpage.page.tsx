import { Form, Field } from "react-final-form";
import { Link, useNavigate } from "react-router-dom";
import { FormForConfirmPasswordType } from "../../Models";
import { InputComponent, ButtonComponent } from "../../stories";
import {
  EMAIL_MESSAGE,
  EMAIL_VALIDATION_MESSAGE,
  PASSWORD_MATCH_MESSAGE,
  PASSWORD_MESSAGE,
  PASSWORD_VALIDATION_MESSAGE,
} from "../../Util/constants";
import {
  POKEMON_LIST_PAGE_ROUTE,
  REGISTER_PAGE_ROUTE,
} from "../../Util/routes";
import { RegexOfEmail, RegexOfPassword } from "../../Util/Validation";
import StyleRegisterPage from "./Registerpage.module.scss";

function Registerpage() {
  const navigate = useNavigate();

  const onSubmit = (e: FormForConfirmPasswordType) => {
    const newUser = { email: e.useremail, password: e.password };
    const oldUsers = localStorage.getItem("users");

    if (oldUsers) {
      const oldUserWithNewUser = JSON.parse(oldUsers);
      oldUserWithNewUser.push(newUser);
      localStorage.setItem("users", JSON.stringify(oldUserWithNewUser));
    } else {
      const newUsers: FormForConfirmPasswordType[] = [];
      newUsers.push(newUser);
      localStorage.setItem("users", JSON.stringify(newUsers));
    }

    navigate("/");
  };

  const validate = (e?: FormForConfirmPasswordType) => {
    const errors: FormForConfirmPasswordType = {};

    if (!e?.useremail) {
      errors.useremail = `${EMAIL_MESSAGE}`;
    } else {
      if (
        RegexOfEmail.test(e.useremail) === false ||
        e.useremail.length <= 4 ||
        e.useremail.length > 100
      ) {
        errors.useremail = `${EMAIL_VALIDATION_MESSAGE}`;
      }
    }

    if (!e?.password) {
      errors.password = `${PASSWORD_MESSAGE}`;
    } else {
      if (
        RegexOfPassword.test(e.password) === false ||
        e.password.length <= 4 ||
        e.password.length >= 16
      ) {
        errors.password = `${PASSWORD_VALIDATION_MESSAGE}`;
      }
    }

    if (!e?.confirmpassword) {
      errors.confirmpassword = `${PASSWORD_MESSAGE}`;
    } else {
      if (e?.password !== e?.confirmpassword) {
        errors.confirmpassword = `${PASSWORD_MATCH_MESSAGE}`;
      }
    }

    return errors;
  };

  return (
    <div className={StyleRegisterPage.register}>
      <div className={StyleRegisterPage.register__form}>
        <h2>Register</h2>
        <Form
          onSubmit={onSubmit}
          validate={validate}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <div className={StyleRegisterPage.register__formcontrol}>
                <Field name="useremail">
                  {({ input, meta }) => (
                    <div>
                      <label>Email</label>
                      <InputComponent id="Email" type="email" {...input} />
                      {meta.error && meta.touched && (
                        <span className={StyleRegisterPage.error}>
                          {meta.error}
                        </span>
                      )}
                    </div>
                  )}
                </Field>
              </div>

              <div className={StyleRegisterPage.register__formcontrol}>
                <Field name="password">
                  {({ input, meta }) => (
                    <div>
                      <label>Password</label>
                      <InputComponent
                        id="Passowrd"
                        type="password"
                        {...input}
                      />
                      {meta.error && meta.touched && (
                        <span className={StyleRegisterPage.error}>
                          {meta.error}
                        </span>
                      )}
                    </div>
                  )}
                </Field>
              </div>

              <div className={StyleRegisterPage.register__formcontrol}>
                <Field name="confirmpassword">
                  {({ input, meta }) => (
                    <div>
                      <label>Confirm Password</label>
                      <InputComponent
                        id="Confirm Password"
                        type="password"
                        {...input}
                      />
                      {meta.error && meta.touched && (
                        <span className={StyleRegisterPage.error}>
                          {meta.error}
                        </span>
                      )}
                    </div>
                  )}
                </Field>
              </div>

              <div className={StyleRegisterPage.register__formcontrol}>
                <ButtonComponent
                  label={"Register"}
                  onClick={() => {
                    navigate(REGISTER_PAGE_ROUTE);
                  }}
                />
              </div>
            </form>
          )}
        />

        <Link
          to={POKEMON_LIST_PAGE_ROUTE}
          className={StyleRegisterPage.register__link}
        >
          Login
        </Link>
      </div>
    </div>
  );
}

export default Registerpage;
