import { useState } from "react";
import { Routes, Route } from "react-router-dom";
import {
  Loginpage,
  PokemonDetails,
  PokemonListing,
  Registerpage,
} from "./Pages";
import {
  POKEMON_DETAILS_PAGE_ROUTE,
  POKEMON_LIST_PAGE_ROUTE,
  REGISTER_PAGE_ROUTE,
} from "./Util/routes";
import "/home/mansi/pokemon-app/src/Style/_abstract.scss";

function App() {
  const localData = localStorage.getItem("login");
  const [loginState, setLoginState] = useState(
    localData === "true" ? true : false
  );

  return (
    <>
      <Routes>
        {loginState ? (
          <>
            <Route
              path={POKEMON_LIST_PAGE_ROUTE}
              element={<PokemonListing />}
            />
          </>
        ) : (
          <>
            <Route
              path={POKEMON_LIST_PAGE_ROUTE}
              element={
                <Loginpage
                  loginState={loginState}
                  setLoginState={setLoginState}
                />
              }
            />
            <Route path={REGISTER_PAGE_ROUTE} element={<Registerpage />} />
          </>
        )}
        <Route path={POKEMON_DETAILS_PAGE_ROUTE} element={<PokemonDetails />} />
      </Routes>
    </>
  );
}

export default App;
