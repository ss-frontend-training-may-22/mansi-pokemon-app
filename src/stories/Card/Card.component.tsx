import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getData } from "../../APIs";
import ButtonComponent from "../Button/Button.component";
import { StyledCard } from "./Card.styled";

type CardComponentProps = {
  name: string;
  url: string;
  onClick: React.MouseEventHandler;
};

type pokemonDetailsData = {
  abilities: {
    ability: {
      name: string;
    };
  }[];
  height: number;
  weight: number;
  sprites: {
    other: {
      dream_world: {
        front_default: string;
      };
    };
  };
};

export default function CardComponent({
  name,
  url,
  onClick,
}: CardComponentProps) {
  const [pokemonInfo, setPokemonInfo] = useState<pokemonDetailsData>();

  useEffect(() => {
    getData(url).then((res) => setPokemonInfo({ ...pokemonInfo, ...res.data }));
  }, []);

  return (
    <>
      <StyledCard onClick={onClick} className="card">
        <div className="card__layout">
          <div className="card__image">
            <img
              src={pokemonInfo?.sprites.other.dream_world.front_default}
              alt=""
            />
          </div>
          <div className="card__information">
            <div>
              <label>Name:</label>
              <span>{name}</span>
            </div>
            <div>
              <label>Height:</label>
              <span>{pokemonInfo?.height}</span>
            </div>
            <div>
              <label>Weight:</label>
              <span>{pokemonInfo?.weight}</span>
            </div>
            <div>
              <label>List of abilites:</label>
            </div>
          </div>
        </div>
        <div className="card__button">
          {pokemonInfo?.abilities.map((item) => (
            <ButtonComponent
              label={item.ability.name}
              key={item.ability.name}
            />
          ))}
        </div>
      </StyledCard>
    </>
  );
}

CardComponent.defaultProps = {
  id: null,
  name: "",
  url: "",
  onClick: { onclick },
};
