import { ComponentStory, ComponentMeta } from "@storybook/react";
import CardComponent from "./Card.component";

export default {
  title: "Card",
  component: CardComponent,
  argTypes: {
    onClikc: { action: "Clicked" },
  },
} as ComponentMeta<typeof CardComponent>;

const Template: ComponentStory<typeof CardComponent> = (args) => (
  <CardComponent {...args} />
);

export const primary = Template.bind({});
primary.args = {
  name: "bulbasaur",
  url: "https://pokeapi.co/api/v2/pokemon/1/",
};
