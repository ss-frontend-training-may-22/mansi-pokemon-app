import { Card } from "@mui/material";
import styled from "styled-components";

export const StyledCard = styled(Card)`
  font-family: "Roboto, sans-serif";
  overflow: hidden;
  height: 268px;
  box-shadow: rgba(0, 0, 0, 0.08) 0px 0px 20px, rgba(0, 0, 0, 0.07) 0px 0px 40px;
  border-radius: 5px;
  padding: 20px;

  .card {
    &__layout {
      display: flex;
      justify-content: space-between;
    }

    &__image {
      float: left;

      img {
        height: 100px;
      }
    }

    &__information {
      float: right;
      margin-top: 80px;

      label {
        font-weight: bold;
        font-size: 18px;
      }
      span {
        font-size: 16px;
      }
    }

    &__button {
      text-align: center;
      margin-top: 14px;

      button {
        margin-right: 5px;
      }
    }
  }
`;
