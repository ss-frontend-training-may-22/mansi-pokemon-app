import { StyledButton } from "./Button.styled";

type ButtonComponentProps = {
  label: string;
  onClick: React.MouseEventHandler;
};

export default function ButtonComponent({
  label,
  onClick,
}: ButtonComponentProps) {
  return (
    <StyledButton onClick={onClick} type="submit">
      {label}
    </StyledButton>
  );
}

ButtonComponent.defaultProps = {
  label: "",
  onClick: () => {},
};
