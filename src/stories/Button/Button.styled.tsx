import styled from "styled-components";
import Theme from "../../Theme";

export const StyledButton = styled.button`
  padding: 10px;
  border: 1px solid;
  background-color: ${Theme.colors.primary};
  color: ${Theme.colors.whitecolor};
`;
