import { ComponentStory, ComponentMeta } from "@storybook/react";
import ButtonComponent from "./Button.component";

export default {
  title: "Button",
  component: ButtonComponent,
  argTypes: {
    onClick: { action: "Clicked" },
  },
} as ComponentMeta<typeof ButtonComponent>;

const Template: ComponentStory<typeof ButtonComponent> = (args) => (
  <ButtonComponent {...args} />
);

export const primary = Template.bind({});
primary.args = {
  label: "overgrow",
};
