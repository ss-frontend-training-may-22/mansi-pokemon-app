import { StyledInput } from "./Input.styled";

type InputTypes = {
  type: string;
  id: string;
  required: boolean;
  placeholder: string;
  onBlur: (
    event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement, Element>
  ) => void;
  onChange?: (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  onFocus?: () => void;
};

export default function InputComponent({
  type,
  id,
  required,
  placeholder,
  onBlur,
  onChange,
  onFocus,
}: InputTypes) {
  return (
    <StyledInput
      id={id}
      type={type}
      required={required}
      placeholder={placeholder}
      onChange={onChange}
      onBlur={onBlur}
      onFocus={onFocus}
    />
  );
}

InputComponent.defaultProps = {
  id: "loginEmail",
  type: "email",
  required: true,
  placeholder: "Enter valid email",
};
