import { InputBase } from "@mui/material";
import styled from "styled-components";
import Theme from "../../Theme";

export const StyledInput = styled(InputBase)`
  border: 1px solid ${Theme.colors.primary};
  padding: 5px;
  font-size: 12px;
  border-radius: 5px;
`;
