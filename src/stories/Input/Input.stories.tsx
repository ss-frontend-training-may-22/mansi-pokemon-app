import { ComponentMeta, ComponentStory } from "@storybook/react";
import InputComponent from "./Input.component";

export default {
  title: "Input",
  component: InputComponent,
} as ComponentMeta<typeof InputComponent>;

const Template: ComponentStory<typeof InputComponent> = (args) => (
  <InputComponent {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  id: "email",
  type: "email",
  required: true,
  placeholder: "Enter valid email",
};

export const EmailInput = Template.bind({});
EmailInput.args = {
  id: "password",
  type: "password",
  required: true,
  placeholder: "Enter password",
};

export const SearchInput = Template.bind({});
SearchInput.args = {
  id: "search",
  type: "search",
  required: true,
  placeholder: "Search ..",
};
