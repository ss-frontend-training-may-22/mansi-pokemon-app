import styled from "styled-components";

export const StyleSelect = styled.div`
  display: block;

  .selectComponent {
    padding: 3px 10px;
  }

  .css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input {
    padding: 5.5px 17px !important;
  }
`;
