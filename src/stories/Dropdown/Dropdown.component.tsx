import React from "react";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { StyleSelect } from "./Dropdown.styled";

type countType = {
  count: number;
  onChange: (event: SelectChangeEvent<string>, child: React.ReactNode) => void;
};

export default function DropdownComponent({ count, onChange }: countType) {
  return (
    <StyleSelect>
      <Select onChange={onChange} className="selectComponent">
        <MenuItem value={10}>10</MenuItem>
        <MenuItem value={20}>20</MenuItem>
        <MenuItem value={30}>30</MenuItem>
      </Select>
    </StyleSelect>
  );
}
