import { ComponentStory, ComponentMeta } from "@storybook/react";
import DropdownComponent from "./Dropdown.component";

export default {
  title: "Dropdown",
  component: DropdownComponent,
  argTypes: {
    onChange: { action: "Clicked" },
  },
} as ComponentMeta<typeof DropdownComponent>;

const Template: ComponentStory<typeof DropdownComponent> = (args) => (
  <DropdownComponent {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  count: 10,
};
