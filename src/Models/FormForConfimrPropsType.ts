export type FormForConfirmPasswordType = {
  useremail?: string;
  password?: string;
  confirmpassword?: string;
};
