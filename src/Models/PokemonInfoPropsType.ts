export type PokemonInfoPropsType = {
  abilities: {
    ability: {
      name: string;
    };
  }[];
  height: number;
  weight: number;
  base_experience: number;
  order: number;
  sprites: {
    other: {
      dream_world: {
        front_default: string;
      };
    };
  };
};
