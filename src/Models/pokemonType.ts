import { PokemonInfoPropsType } from "./PokemonInfoPropsType";
import { PokemonPropsType } from "./PokemonPropsType";

export type PokemonType = {
  pokemonList: PokemonPropsType[];
  pokemonInfo: PokemonInfoPropsType;
};
