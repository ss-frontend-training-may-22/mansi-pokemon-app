import { PokemonPropsType } from "./PokemonPropsType";
import { PokemonInfoPropsType } from "./PokemonInfoPropsType";
import { PokemonType } from "./pokemonType";
import { FormType } from "./FormPropsType";
import { UserDataType } from "./UserDataPropsType";
import { FormForConfirmPasswordType } from "./FormForConfimrPropsType";

export type {
  PokemonPropsType,
  PokemonInfoPropsType,
  PokemonType,
  FormType,
  UserDataType,
  FormForConfirmPasswordType,
};
