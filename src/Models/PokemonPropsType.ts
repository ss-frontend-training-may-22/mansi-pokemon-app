export type PokemonPropsType = {
  id: number;
  name: string;
  url: string;
};
