import axios, { AxiosRequestConfig } from "axios";

export const get = <T,>(url: string, data?: AxiosRequestConfig) => {
  return axios.get(url, data);
};
