export const BASE_URL = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=50";
export const EMAIL_MESSAGE = "Enter Email";
export const EMAIL_VALIDATION_MESSAGE = "Enter valid Email";
export const PASSWORD_MESSAGE = "Enter Password";
export const PASSWORD_MATCH_MESSAGE = "Password Not Match";
export const PASSWORD_VALIDATION_MESSAGE =
  "Password length is 5 to 15 character and one number must be there";
