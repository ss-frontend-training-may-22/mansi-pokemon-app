export const RegexOfEmail =
  /^[a-zA-Z0-9.!#$%&' * +/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export const RegexOfPassword = /([a-zA-Z]*[0-9]+)/;
