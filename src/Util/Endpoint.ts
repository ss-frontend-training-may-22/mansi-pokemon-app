import { BASE_URL } from "./constants";

export const PokemomInfoEndPoint = (id: number) => `${BASE_URL}${id}`;
